<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Preved, Medved!</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">  
</head>
<body>
    <!-- Static navbar -->
    <nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">Preved, Medved!</a>
        </div>        
      </div>
    </nav>    

    <div class="container">
        <?php if ($m != '') : ?>
            <div class="alert alert-<?php echo $m == 'Add OK' ? 'success' : 'info'; ?>"><?php echo htmlspecialchars($m); ?></div>
        <?php endif; ?>    

        <div class="row">
            <div class="col-md-6">
                <form action="/add" method="post" class="form-inline">
                  <div class="form-group">
                    <input type="text" class="form-control" name="number" placeholder="Phone number*" required="required">
                  </div>          
                  <button type="submit" class="btn btn-default">Add number</button>
                </form>
            </div>
            
            <div class="col-md-6">
                <form action="/activate" method="post" class="form-inline">
                  <div class="form-group">
                    <input type="text" class="form-control" name="number" placeholder="Phone number*" required="required">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" name="code" placeholder="Code*" required="required">
                  </div>
                  <button type="submit" class="btn btn-default">Activate</button>
                </form>
            </div>
        </div>    
        <hr>

        <table class="table table-bordered">
        <tr>
            <th>Id</th>
            <th>Number</th>
            <th>Code</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        <?php foreach($numbers as $number) : ?>
            <tr>
                <td><?php echo $number['id']; ?></td>
                <td><?php echo $number['number']; ?></td>
                <td><?php echo $number['code']; ?></td>
                <td><?php echo $number['status']; ?></td>
                <td><a href="/delete?id=<?php echo $number['id']; ?>" class="btn btn-danger btn-small" onclick="return confirm ('Are you sure?');">Delete</a></td>
            </tr>
        <?php endforeach; ?>
        </table>


    </div> <!-- /container -->

    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  </body>
</html>