<?php

require 'vendor/autoload.php';

$app = new \Slim\Slim();

$db = new PDO('sqlite:db.sqlite3');

function isApiKeyValid($key) {
    return $key === '12345';
}

$app->get('/', function () use ($app, $db) {
    $app->render('index.php', [
        'numbers' => $db->query("SELECT * FROM numbers")->fetchAll(),
        'm' => $app->request->get('m'),
    ]);
});

$app->post('/add', function () use ($app, $db)  {
    $number = $app->request->post('number');
    if (empty($number)) {
        $app->response->redirect('/?m=Number empty', 302);
        return;
    }

    if (intval($db->query("SELECT id FROM numbers WHERE number='".addslashes($number)."'")->fetchColumn()) > 0) {
        $app->response->redirect('/?m=This number exists', 302);
        return;
    }

    $db->query("INSERT INTO numbers (number, code, status) VALUES ('".addslashes($number)."', ".(1000+mt_rand(1,8999)).", 'new')");
    $app->response->redirect('/?m=Add OK', 302);
});

$app->post('/activate', function () use ($app, $db)  {
    $number = strval($app->request->post('number'));
    if (empty($number)) {
        $app->response->redirect('/?m=Number empty', 302);
        return;
    }
    $code = intval($app->request->post('code'));
    if ($code <= 0) {
        $app->response->redirect('/?m=Code empty', 302);
        return;
    }

    if (!$obj = $db->query("SELECT id FROM numbers WHERE number='".addslashes($number)."' AND code=$code LIMIT 1")->fetchObject()) {
        $app->response->redirect('/?m=Code or number wrong', 302);
        return;
    }
    
    $db->query("UPDATE numbers SET status='confirmed' WHERE id=".$obj->id);
    $app->response->redirect('/?m=Activation OK', 302);
});

$app->get('/delete', function () use ($app, $db)  {
    $db->query("DELETE FROM numbers WHERE id=".intval($app->request->get('id')));
    $app->response->redirect('/', 302);
});

/** API */

$app->get('/api/get', function () use ($app, $db)  {
    $app->response->headers->set('Content-Type', 'application/json');

    if (!isApiKeyValid($app->request->get('key'))) {
        $app->response->setBody(json_encode([
            'status' => 'error',
            'error' => 'Wrong api key'
        ]));
        return;
    }

    $app->response->setBody(json_encode($db->query("SELECT number, code FROM numbers WHERE status='new'")->fetchAll(PDO::FETCH_ASSOC)));
});

$app->get('/api/update', function () use ($app, $db)  {
    $app->response->headers->set('Content-Type', 'application/json');

    if (!isApiKeyValid($app->request->get('key'))) {
        $app->response->setBody(json_encode([
            'status' => 'error',
            'error' => 'Wrong api key'
        ]));
        return;
    }

    $number = strval($app->request->get('number'));
    if (empty($number)) {
        $app->response->setBody(json_encode([
            'status' => 'error',
            'error' => 'Number empty'
        ]));
        return;
    }    

    if (!$obj = $db->query("SELECT id FROM numbers WHERE number='".addslashes($number)."' LIMIT 1")->fetchObject()) {
        $app->response->setBody(json_encode([
            'status' => 'error',
            'error' => 'Wrong number'
        ]));
        return;
    }

    $db->query("UPDATE numbers SET status='wait confirmation' WHERE id=".$obj->id);
    
    $app->response->setBody(json_encode(['status'=>'ok']));
});

$app->run();