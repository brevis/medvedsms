package com.medved.sms.medvedsms;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

    private Worker worker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadSettings();
        startWorker();

        final Button saveSettingsButton = (Button) findViewById(R.id.saveSettingsButton);
        saveSettingsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                saveSettings();

                try {
                    worker.stop();
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
                startWorker();
            }
        });

        final Button exitButton = (Button) findViewById(R.id.exitButton);
        exitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    worker.stop();
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
                finish();
                System.exit(0);
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            worker.stop();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void loadSettings() {
        EditText serverUrlEdit = (EditText)findViewById(R.id.serverUrlEditText);
        EditText apiKeyEdit = (EditText)findViewById(R.id.apiKeyEditText);
        EditText workerIntervalEdit = (EditText)findViewById(R.id.workerIntervalEditText);

        SharedPreferences settings = getSharedPreferences("backend_settings", 0);
        serverUrlEdit.setText(settings.getString("serverUrl", ""));
        apiKeyEdit.setText(settings.getString("apiKey", ""));
        workerIntervalEdit.setText(String.valueOf(settings.getInt("workerInterval", 15)));
    }

    public void saveSettings() {
        EditText serverUrlEdit = (EditText)findViewById(R.id.serverUrlEditText);
        EditText apiKeyEdit = (EditText)findViewById(R.id.apiKeyEditText);
        EditText workerIntervalEdit = (EditText)findViewById(R.id.workerIntervalEditText);

        String serverUrl = serverUrlEdit.getText().toString();
        String apiKey = apiKeyEdit.getText().toString();
        Integer workerInterval = Integer.valueOf(workerIntervalEdit.getText().toString());
        String errorMessage = "";
        if (serverUrl.equals("")) errorMessage += "Server URL is empty\n";
        if (apiKey.equals("")) errorMessage += "API Key is empty\n";
        if (workerInterval < 1) errorMessage += "Wrong worker interval\n";
        if (!errorMessage.equals("")) {
            showMessage("Error", errorMessage);
            return;
        }

        SharedPreferences settings = getSharedPreferences("backend_settings", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("serverUrl", serverUrl);
        editor.putString("apiKey", apiKey);
        editor.putInt("workerInterval", workerInterval);
        editor.commit();
    }

    public void startWorker() {
        EditText serverUrlEdit = (EditText)findViewById(R.id.serverUrlEditText);
        EditText apiKeyEdit = (EditText)findViewById(R.id.apiKeyEditText);
        EditText workerIntervalEdit = (EditText)findViewById(R.id.workerIntervalEditText);

        String serverUrl = serverUrlEdit.getText().toString();
        String apiKey = apiKeyEdit.getText().toString();
        Integer workerInterval = Integer.valueOf(workerIntervalEdit.getText().toString());

        try {
            worker = new Worker(this, serverUrl, apiKey, workerInterval);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public void showMessage(final String title, final String message) {
        this.runOnUiThread(new Runnable() {
            public void run() {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle(title)
                        .setMessage(message)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }

    public void addLogMessage(final String message) {
        this.runOnUiThread(new Runnable() {
            public void run() {
                TextView logTextView = (TextView) findViewById(R.id.logTextView);
                logTextView.append(message + "\n");
            }
        });
    }

}
