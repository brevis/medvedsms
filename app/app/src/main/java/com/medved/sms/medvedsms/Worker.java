package com.medved.sms.medvedsms;

import android.telephony.SmsManager;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;

public class Worker extends TimerTask {
    private MainActivity activity;
    private Timer processTimer = new Timer();
    private int t = 0;

    protected String serverUrl;
    protected String apiKey;

    public Worker(MainActivity activity, String serverUrl, String apiKey, Integer workerInterval) throws Exception {
        this.activity = activity;
        this.serverUrl = serverUrl;
        this.apiKey = apiKey;
        if (workerInterval < 1) {
            throw new Exception("Can't start worker: Wrong worker interval");
        }
        processTimer.schedule(this, 1000, workerInterval * 1000);
    }

    public void stop() {
        processTimer.cancel();
        processTimer.purge();
    }

    @Override
    public void run() {
        this.activity.addLogMessage("Connect to server...");

        String parsedString = "";

        String apiUrl = this.serverUrl;
        if (!apiUrl.endsWith("/")) apiUrl += "/";
        apiUrl += "api/get?key=" + this.apiKey;

        try {
            URL url = new URL(apiUrl);
            URLConnection conn = url.openConnection();

            HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();

            InputStream is = httpConn.getInputStream();
            parsedString = convertinputStreamToString(is);
        } catch (Exception e) {
            e.printStackTrace();
        }


        try{
            JSONArray json = new JSONArray(parsedString);
            if (json.length() <1 ) {
                this.activity.addLogMessage("No \"new\" numbers");
            }
            for(int i = 0; i < json.length(); i++){
                String number = json.getJSONObject(i).getString("number");
                String code = json.getJSONObject(i).getString("code");
                String logMessage = number + "; sms: ";
                if (sendSms(number, code)) {
                    logMessage += "ok; update status: ";
                    if (updateNumberStatus(number)) {
                        logMessage += "ok";
                    } else {
                        logMessage += "error";
                    }
                } else {
                    logMessage += "error";
                }
                this.activity.addLogMessage(logMessage);
            }

        } catch (Exception e) {
            this.activity.addLogMessage("Error parse json");
            System.out.println(e.getMessage());
        }
    }

    public boolean sendSms(String number, String code) {
        String message = this.serverUrl + ":\nCode: " + code;
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(number, null, message, null, null);
            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

    public boolean updateNumberStatus(String number) {
        String apiUrl = this.serverUrl;
        if (!apiUrl.endsWith("/")) apiUrl += "/";
        apiUrl += "api/update?key=" + this.apiKey;
        apiUrl += "&number=" + number;

        try {
            URL url = new URL(apiUrl);
            URLConnection conn = url.openConnection();

            HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();

            InputStream is = httpConn.getInputStream();
            JSONObject json = new JSONObject(convertinputStreamToString(is));
            return !json.has("error");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String convertinputStreamToString(InputStream ists)
            throws IOException {
        if (ists != null) {
            StringBuilder sb = new StringBuilder();
            String line;

            try {
                BufferedReader r1 = new BufferedReader(new InputStreamReader(
                        ists, "UTF-8"));
                while ((line = r1.readLine()) != null) {
                    sb.append(line).append("\n");
                }
            } finally {
                ists.close();
            }
            return sb.toString();
        } else {
            return "";
        }
    }


}
